package basic.security.service;

import basic.security.model.Post;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService, CommandLineRunner {

    private List<Post> posts = new ArrayList<>();



    @Override
    public void run(String... args) throws Exception {

        this.posts.add(new Post("Hello, World!1","hello-world1","Welcome to my new blog!1","Author 1"));
        this.posts.add(new Post("Hello, World!2","hello-world2","Welcome to my new blog!2","Author 2"));
        this.posts.add(new Post("Hello, World!3","hello-world3","Welcome to my new blog!3","Author 3"));
        this.posts.add(new Post("Hello, World!4","hello-world4","Welcome to my new blog!4","Author 4"));
    }

    @Override
    public List<Post> findAllPosts() {
        return this.posts;
    }
}
