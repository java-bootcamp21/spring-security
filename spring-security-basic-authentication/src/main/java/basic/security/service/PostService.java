package basic.security.service;

import basic.security.model.Post;

import java.util.List;

public interface PostService {

    List<Post> findAllPosts();

}
