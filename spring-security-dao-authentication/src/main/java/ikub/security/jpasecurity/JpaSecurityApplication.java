package ikub.security.jpasecurity;

import ikub.security.jpasecurity.model.Post;
import ikub.security.jpasecurity.model.User;
import ikub.security.jpasecurity.repository.PostRepository;
import ikub.security.jpasecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class JpaSecurityApplication implements CommandLineRunner{

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(JpaSecurityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		userRepository.save(new User("user",passwordEncoder.encode("password"),"ROLE_USER"));
		userRepository.save(new User("admin",passwordEncoder.encode("password"),"ROLE_USER,ROLE_ADMIN"));
		postRepository.save(new Post("Hello, World!","hello-world","Welcome to my new blog!","Author 1"));
	}
}
