package jwt.security.controller;

import jwt.security.config.AuthService;
import jwt.security.dto.LoginRequestDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public String token(@RequestBody LoginRequestDto req) {
        return "Bearer ".concat(authService.generateToken(req));
    }

}