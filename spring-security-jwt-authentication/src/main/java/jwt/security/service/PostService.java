package jwt.security.service;

import jwt.security.model.Post;

import java.util.List;

public interface PostService {

    List<Post> findAll();
}
