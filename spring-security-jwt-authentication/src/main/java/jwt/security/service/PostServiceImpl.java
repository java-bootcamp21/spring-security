package jwt.security.service;

import jwt.security.model.Post;
import jwt.security.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postService;

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public List<Post> findAll() {
        return postService.findAll();
    }
}
