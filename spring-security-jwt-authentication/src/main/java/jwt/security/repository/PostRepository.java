package jwt.security.repository;

import jwt.security.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;

public interface PostRepository extends JpaRepository<Post,Long> {

}
