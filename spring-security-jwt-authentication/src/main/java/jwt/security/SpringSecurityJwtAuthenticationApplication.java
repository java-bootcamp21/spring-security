package jwt.security;

import jwt.security.model.Post;
import jwt.security.model.User;
import jwt.security.repository.PostRepository;
import jwt.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringSecurityJwtAuthenticationApplication implements CommandLineRunner{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityJwtAuthenticationApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        userRepository.save(new User("user",passwordEncoder.encode("password"),"USER"));
        userRepository.save(new User("admin",passwordEncoder.encode("password"),"USER,ADMIN"));
        postRepository.save(new Post("Hello, World!","hello-world","Welcome to my new blog!","Author 1"));
    }
}
